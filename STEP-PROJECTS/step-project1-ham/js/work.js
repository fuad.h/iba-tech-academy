const navWork = document.querySelectorAll(".work-nav>a");
const hr = document.querySelector(".work-hr");
const workImage = document.querySelectorAll(".work-image>img");
const btn = document.querySelector(".loadBtn");

let b = 11;

const hr2 = document.createElement("hr");
hr2.classList.add("work-hr");
navWork.forEach(item => {
  item.addEventListener("click", e => {
    e.preventDefault();

    for (let i of navWork) {
      if (i.classList.contains("work-active")) {
        i.classList.remove("work-active");
      }
      e.target.classList.add("work-active");
    }

    for (let i of navWork) {
      if (i.contains(hr)) {
        i.removeChild(hr);
      }
      e.target.appendChild(hr2);
    }

    const type = e.target.dataset.filterby;

    if (type) {
      workImage.forEach(img => {
        if (!img.classList.contains(type)) {
          img.classList.add("image-none");
        } else {
          img.classList.remove("image-none");
        }
      });
      btn.classList.add("image-none");
      b = 11;
    } else {
      increment(11);
      btn.classList.remove("image-none");
    }
  });
});

btn.addEventListener("click", () => {
  if (b < 36) {
    b = b + 12;
    if (b == 35) {
      btn.classList.add("image-none");
    }

    increment(b);
  }
});

function increment(b) {
  workImage.forEach(item => {
    item.classList.add("image-none");
  });

  for (let i = 0; i <= b; i++) {
    let item = workImage[i];
    item.classList.remove("image-none");
  }
}
