const navService = document.querySelectorAll(".service-nav > a");
const navDiv = document.createElement("div");
const serviceDiv = document.querySelector(".service-nav > a > div");
const serviceImageText = document.querySelectorAll(".service-img-text");

for (let item of navService) {
  item.addEventListener("click", (e) => {
    e.preventDefault();

    navService.forEach((element) => {
      element.classList.remove("service-nav-active");
      if (element.contains(serviceDiv)) {
        element.removeChild(serviceDiv);
      }
    });

    e.target.classList.add("service-nav-active");
    e.target.appendChild(serviceDiv);

    const type = e.target.dataset.filterby;

    serviceImageText.forEach((e) => {
      if (e.classList.contains(type)) {
        e.classList.remove("service-passive");
      } else {
        e.classList.add("service-passive");
      }
    });
  });
}
