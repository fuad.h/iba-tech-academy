const navHeader = document.querySelectorAll(".header-nav > a");
const img = document.querySelector(".header-nav > a> img");
for (let item of navHeader) {
  item.addEventListener("click", e => {
    if (!item.contains(img)) {
      navHeader.forEach(element => {
        element.classList.remove("header-nav-active");
      });

      e.target.classList.add("header-nav-active");
    }
  });
}
