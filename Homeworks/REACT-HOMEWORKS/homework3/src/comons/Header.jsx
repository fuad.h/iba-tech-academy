import React, { useState, useContext } from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

import { CartContext } from "../context/cart";
export const Header = () => {
  const [isActive, setIsActive] = useState("product");
  const { cartProduct } = useContext(CartContext);

  return (
    <StyledHeader>
      <NavLink
        exact
        to="/"
        className={isActive == "product" ? "active" : ""}
        onClick={() => setIsActive("product")}
      >
        Product
      </NavLink>
      <NavLink
        to="/cart"
        className={isActive == "cart" ? "active" : ""}
        onClick={() => setIsActive("cart")}
      >
        Cart <b>{!!cartProduct.length && cartProduct.length}</b>
      </NavLink>
      <NavLink
        to="/favorite"
        className={isActive == "favorite" ? "active" : ""}
        onClick={() => setIsActive("favorite")}
      >
        Favorite
      </NavLink>
    </StyledHeader>
  );
};

const StyledHeader = styled.header`
  text-align: center;
  margin-top: 40px;
  a {
    text-decoration: none;
    font-size: 15px;
    padding: 10px 30px;
    color: black;
    border: 1px solid lightgrey;
    margin-left: 30px;
    border-radius: 7px;
    text-transform: uppercase;
    &.active {
      background-color: #4d4d4d;
      color: white;
      border: 2px solid grey;
    }
  }
`;
