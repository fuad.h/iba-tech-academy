import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { ProductContextProvider } from "./context/product";
import { CartContextProvider } from "./context/cart";
import { Product, Cart, Favorite } from "./pages";
import { Layout } from "./comons";

function App() {
  return (
    <Router>
      <ProductContextProvider>
        <CartContextProvider>
          <Layout>
            <Switch>
              <Route exact path="/" component={Product} />
              <Route path="/cart" component={Cart} />
              <Route path="/favorite" component={Favorite} />
            </Switch>
          </Layout>
        </CartContextProvider>
      </ProductContextProvider>
    </Router>
  );
}

export default App;
