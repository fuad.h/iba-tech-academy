import React, { createContext, useState, useEffect } from "react";

export const ProductContext = createContext();

export const ProductContextProvider = ({ children }) => {
  const [data, setData] = useState([]);

  const getData = async () => {
    const res = await fetch("http://localhost:3001/products");
    const json = await res.json();
    setData(json);
  };

  useEffect(() => {
    getData();
  }, []);

  const setFavorite = (number, favorite) => {
    setData(item =>
      item.map(e => {
        if (e.number === number) {
          return {
            ...e,
            favorite: !favorite
          };
        }
        return e;
      })
    );
  };

  const removeFromCart = number => {
    setData(item => item.filter(e => e.number !== number));
  };
  return (
    <ProductContext.Provider value={{ data, setFavorite, removeFromCart }}>
      {children}
    </ProductContext.Provider>
  );
};
