import React, { createContext, useReducer } from "react";

export const CartContext = createContext();

const InitialState = {
  cartProduct: []
};

function reducer(state, { type, payload }) {
  switch (type) {
    case "add_to_cart":
      if (state.cartProduct.some(item => item.number == payload.number)) {
        return {
          cartProduct: state.cartProduct.map(item => {
            if (item.number == payload.number) {
              return {
                ...item,
                count: item.count + 1
              };
            }
            return item;
          })
        };
      } else {
        return { cartProduct: [...state.cartProduct, payload] };
      }

    case "remove_from_cart":
      if (payload.count === 1) {
        return {
          cartProduct: state.cartProduct.filter(
            item => item.number !== payload.number
          )
        };
      } else {
        return {
          cartProduct: state.cartProduct.map(item => {
            if (item.number == payload.number) {
              return {
                ...item,
                count: item.count - 1
              };
            }
            return item;
          })
        };
      }

    default:
      return state;
  }
}

const addToCartAC = payload => ({
  type: "add_to_cart",
  payload
});

const removeFromCartAC = payload => ({
  type: "remove_from_cart",
  payload
});
export const CartContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, InitialState);

  const addToCart = product => {
    dispatch(addToCartAC(product));
  };
  const removeFromCart = cart => {
    dispatch(removeFromCartAC(cart));
  };
  return (
    <CartContext.Provider value={{ ...state, addToCart, removeFromCart }}>
      {children}
    </CartContext.Provider>
  );
};
