import React, { useContext } from "react";
import styled from "styled-components";

import { CartContext } from "../../context/cart";
export const Cart = () => {
  const { cartProduct, removeFromCart } = useContext(CartContext);

  return (
    <div style={{ marginTop: 100 }}>
      {cartProduct.map(({ name, path, count, price, number }) => (
        <Main key={number}>
          <img src={path} alt={name} />
          <h1>{name}</h1>
          <h1>${price}</h1>
          <h1>count({count})</h1>
          <button onClick={() => removeFromCart({ number, count })}>X</button>
        </Main>
      ))}
    </div>
  );
};

const Main = styled.div`
  width: 500px;
  margin: 20px auto;
  border-radius: 10px;
  background-color: lightgrey;
  display: flex;
  align-items: center;
  justify-content: space-between;
  img {
    height: 50px;
    width: 50px;
    border-radius: 7px;
  }
  h1 {
    margin: 0 20px 0 20px;
    font-size: 16px;
    text-align: center;
  }
  button {
    background-color: #1e1e20;
    color: white;
    border: none;
    border-radius: 4px;
    margin-right: 10px;
    cursor: pointer;
  }
`;
