import React, { useContext } from "react";
import styled from "styled-components";
import { List } from "../Product/List";

import { ProductContext } from "../../context/product";
export const Favorite = () => {
  const { data } = useContext(ProductContext);
  return (
    <Container>
      <Product>
        {data
          .filter(item => item.favorite === true)
          .map(item => (
            <List key={item.number} {...item} />
          ))}
      </Product>
    </Container>
  );
};

const Container = styled.div`
  width: 630px;
  margin: 10% auto;
`;

const Product = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;
