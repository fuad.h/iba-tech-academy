import React, { useContext } from "react";
import "./style.sass";
import { ProductContext } from "../../../context/product";

import { List } from "../List";

export const ProductCart = () => {
  const { data } = useContext(ProductContext);

  return (
    <div className="container">
      <div className="product-cart">
        {data.map(item => (
          <List key={item.number} {...item} />
        ))}
      </div>
    </div>
  );
};
