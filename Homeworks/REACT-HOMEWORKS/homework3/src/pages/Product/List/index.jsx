import React, { useState, useEffect, useContext } from "react";
import "./style.sass";

import { CartContext } from "../../../context/cart";
import { Star } from "../Star";
import { ProductContext } from "../../../context/product";

export const List = ({ name, price, path, number, color, favorite }) => {
  const [svgColor, setSvgColor] = useState("#000000");

  const { addToCart } = useContext(CartContext);
  const { setFavorite, removeFromCart } = useContext(ProductContext);

  const colorChangeHaneler = () => {
    setFavorite(number, favorite);
  };
  return (
    <div className="product-list">
      <button className="close-btn" onClick={() => removeFromCart(number)}>
        x
      </button>
      <img src={path} alt={name} />
      <div className="wrapper">
        <h3>{name}</h3>
        <p>
          <i>by Artist</i>
        </p>
        <a onClick={colorChangeHaneler} alt={name}>
          <img src="/img/rating.png" alt="rating" />
          <Star color={favorite ? color : "#000000"} />
        </a>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
        <h1>${price}</h1>
        <button
          onClick={() => addToCart({ name, path, price, number, count: 1 })}
        >
          Add to cart
        </button>
      </div>
    </div>
  );
};
