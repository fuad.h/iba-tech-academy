import React, { useState } from "react";
import styled from "styled-components";

import Button from "./components/Button";
import Modal from "./components/Modal";

function App() {
  const [showButton, setShowButton] = useState(true);
  const [firstModal, setFirstModal] = useState(true);

  const buttonHandeler1 = () => {
    if (showButton === true) {
      setShowButton(false);
    } else {
      setShowButton(true);
    }
  };
  const buttonHandeler2 = () => {
    if (showButton === true) {
      setShowButton(false);
      setFirstModal(false);
    } else {
      setShowButton(true);
      setFirstModal(true);
    }
  };

  return (
    <Main>
      <Container className="App">
        {showButton ? (
          <>
            <Button
              backgroundColor="#b3382c"
              text="Open first modal window"
              onClick={buttonHandeler1}
            />
            <Button
              backgroundColor="#b3382c"
              text="Open second  modal window"
              onClick={buttonHandeler2}
            />
          </>
        ) : (
          <>
            {firstModal ? (
              <Modal
                header="Do you want to delete this file?"
                closeButton={true}
                text="Once you delete this file, it won’t be possible to undo this action. 
              Are you sure you want to delete it?"
                action={[
                  <Button
                    backgroundColor="#b3382c"
                    text="Ok"
                    onClick={buttonHandeler1}
                  />,
                  <Button
                    backgroundColor="#b3382c"
                    text="Cancel"
                    onClick={buttonHandeler1}
                  />
                ]}
              />
            ) : (
              <Modal
                header="Do you want to delete this file?"
                closeButton={true}
                text="Aru you alive?"
                action={[
                  <Button
                    backgroundColor="#b3382c"
                    text="Ok"
                    onClick={buttonHandeler2}
                  />,
                  <Button
                    backgroundColor="#b3382c"
                    text="Cancel"
                    onClick={buttonHandeler2}
                  />
                ]}
              />
            )}
          </>
        )}
      </Container>
    </Main>
  );
}

export default App;
const Main = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #312d2d;
`;
const Container = styled.div`
  width: 600px;
  height: 300px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: #e74c3c;
  border-radius: 5px;
`;
