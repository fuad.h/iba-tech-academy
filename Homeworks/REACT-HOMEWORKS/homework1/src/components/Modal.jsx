import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
const Modal = ({ header, closeButton, text, action }) => {
  return (
    <Main>
      <Header>
        <h1>{header}</h1>
        <a href="#">
          <img src="/img/cross.png"></img>
        </a>
      </Header>
      <P>
        <h3>{text}</h3>
      </P>
      <Button>
        {action.map((item, id) => (
          <div key={id}>{item}</div>
        ))}
      </Button>
    </Main>
  );
};

Modal.propType = {
  text: PropTypes.string,
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  action: PropTypes.array
};
export default Modal;
const Main = styled.div`
  width: 100%;
  height: 300px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const Button = styled.div`
  width: 80%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 50px;

  button {
    font-size: 20px;
  }
`;

const Header = styled.header`
  width: 100%;
  height: 80px;
  border-radius: 5px 5px 0 0;
  background: #d44637;
  display: flex;
  justify-content: space-between;
  align-items: center;
  h1 {
    padding-left: 20px;
    font-size: 25px;
    color: #ffffff;
    font-family: "Helvetica Neue";
    font-weight: 500;
  }
  a {
    margin-right: 20px;

    img {
      height: 40px;
      width: 40px;
      object-fit: cover;
    }
  }
`;
const P = styled.div`
  width: 80%;
  margin: 20px auto;
  h3 {
    font-size: 18px;
    line-height: 30px;
    color: #ffffff;
    font-family: "Helvetica Neue";
    text-align: center;
    font-weight: 100;
  }
`;
