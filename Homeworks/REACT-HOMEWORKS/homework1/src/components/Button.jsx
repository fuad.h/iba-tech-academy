import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
const Button = ({ backgroundColor, text, onClick }) => {
  return (
    <>
      <Btn
        style={{
          backgroundColor
        }}
        onClick={onClick}
      >
        {text}
      </Btn>
    </>
  );
};

Button.propType = {
  text: PropTypes.string,
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func
};

export default Button;
const Btn = styled.button`
  padding: 10px 30px;
  min-width: 150px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  font-size: 15px;
  line-height: 30px;
  color: #ffffff;
  font-family: "Helvetica Neue";
  text-align: center;

  &:focus {
    outline: none;
  }
`;
