import React, { useState, useEffect } from "react";
import "./style.sass";
import { getProducts } from "../../API/index";

import { List } from "../List";
import { Modal } from "../Modal";

export const Cart = () => {
  const [data, setData] = useState([]);
  const [value, setValue] = useState(true);

  useEffect(() => {
    (async () => {
      const answer = await getProducts();
      setData(answer);
    })();
  }, []);
  return (
    <>
      {value ? (
        <>
          <div className="container">
            <div className="product-cart">
              {data.map(item => (
                <List
                  value={value}
                  setValue={setValue}
                  key={item.number}
                  {...item}
                />
              ))}
            </div>
          </div>
        </>
      ) : (
        <Modal setValue={setValue} />
      )}
    </>
  );
};
