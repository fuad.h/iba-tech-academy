import React from "react";
import "./style.sass";
export const Modal = ({ setValue }) => {
  return (
    <div className="Main">
      <header>
        <h1>Do you want to save this ?</h1>
        <a href="#">
          <img src="/img/cross.png"></img>
        </a>
      </header>
      <div className="p-style">
        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </h3>
      </div>
      <div className="btn">
        <button onClick={() => setValue(true)}>Ok</button>
        <button onClick={() => setValue(true)}>Cancel</button>
      </div>
    </div>
  );
};
