import React, { useState, useEffect } from "react";
import "./style.sass";

import { Star } from "../Star";
export const List = ({
  name,
  price,
  path,
  number,
  color,
  value,
  setValue,
  favorite
}) => {
  const changeView = () => {
    setValue(false);
  };
  const [svgColor, setSvgColor] = useState("#000000");

  const colorChangeHaneler = async () => {
    if (favorite == false) {
      setSvgColor(color);
    }
  };

  return (
    <div className="product-list">
      <img src={path} alt={name} />
      <div className="wrapper">
        <h3>{name}</h3>
        <p>
          <i>by Artist</i>
        </p>
        <a onClick={colorChangeHaneler} alt={name}>
          <img src="/img/rating.png" alt="rating" />
          <Star color={svgColor} />
        </a>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
        <h1>${price}</h1>
        <button onClick={changeView}>Add to cart</button>
      </div>
    </div>
  );
};
