import React from "react";
import { Cart } from "./components";

function App() {
  return (
    <div className="main">
      <Cart />
    </div>
  );
}

export default App;
