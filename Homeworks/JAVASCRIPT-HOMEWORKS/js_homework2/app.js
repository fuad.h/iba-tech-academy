let checkNumber = number => {
  if (number == null || number == "" || isNaN(+number)) return false;

  return true;
};

let value = null;
while (value == null) {
  let number1 = +prompt("Enter first number", 12);
  let number2 = +prompt("Enter second number", 12);

  const isNumber = !(checkNumber(number1) && checkNumber(number2));

  if (!isNumber) {
    calculate(number1, number2);
    value = number1;
  } else {
    alert("Enter again");
  }
}

function calculate(num1, num2) {
  let operation = prompt("Enter operation :");
  switch (operation) {
    case "+":
      alert(num1 + num2);
      break;
    case "-":
      alert(num1 - num2);
      break;
    case "*":
      alert(num1 * num2);
      break;
    case "/":
      alert(num1 / num2);
      break;
  }
}
