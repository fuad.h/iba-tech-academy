const tabsContainer = document.getElementById("filterby");
const items = document.querySelectorAll(".tabs-items");
const tabs_content = document.getElementById("tabs-content");
const textArea = document.createElement("textarea");
tabsContainer.addEventListener("click", function(event) {
  if (event.target.classList.contains("tabs-title")) {
    const title = event.target;
    const type = title.dataset.filterby;
    const isActive = title.classList.contains("active");

    if (!isActive) {
      document.querySelector(".tabs-title.active").classList.remove("active");
      title.classList.add("active");
      filterByClassName(items, type);
    }
  }
});

tabs_content.addEventListener("click", function(event) {
  const selected_li = event.target;

  if (selected_li.classList.contains("tabs-items")) {
    //console.log(tabs_content);
    changeConent(selected_li, true);
  }
});

function filterByClassName(elements, className) {
  for (let element of elements) {
    if (!element.classList.contains("passive")) {
      element.classList.add("passive");
    }
    if (element.classList.contains(className)) {
      element.classList.remove("passive");
    }
  }
}

function changeConent(element, status) {
  if (status) {
    textArea.classList.add("textArea");
    textArea.value = element.textContent;
    element.replaceWith(textArea);
  }
}
