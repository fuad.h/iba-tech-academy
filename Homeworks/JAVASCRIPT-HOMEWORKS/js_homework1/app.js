const LEES_AGE = "You are not allowed to visit this website";
const AGE_INCLUSIVE = "Are you sure you want to continue?";
const WELCOME = "Welcome";
const CANCEL = "Cancel";

let isAllow = () => {
  let name = prompt("What is your name : ");
  let age = +prompt("What is your age : ");

  const age_under_18 = age < 18;
  const age_between = age < 22;
  age_under_18
    ? alert(LEES_AGE)
    : age_between
    ? confirm(AGE_INCLUSIVE)
      ? alert(WELCOME + " " + name)
      : alert(LEES_AGE)
    : alert(WELCOME + " " + name);
};

isAllow();
