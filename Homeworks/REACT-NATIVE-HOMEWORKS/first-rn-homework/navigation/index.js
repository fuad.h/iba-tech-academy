import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";

import {
  RegularScreen,
  OneTimeScreen,
  NewListScreen,
  UserScreen,
} from "../screens";
import { RegularStack } from "./RegularStack";
import { OneTimeStack } from "./OneTimeStack";

import { Menu } from "../commons";

export const RootDrawer = () => {
  const { Navigator, Screen } = createDrawerNavigator();

  return (
    <NavigationContainer>
      <Navigator
        drawerStyle={{
          width: 230,
        }}
        overlayColor="transparent"
        statusBarAnimation="slide"
        drawerContent={(props) => <Menu {...props} />}
      >
        <Screen name="Regular" component={RegularStack} />
        <Screen name="One Time List" component={OneTimeStack} />
        <Screen name="New List" component={NewListScreen} />
        <Screen name="User Profile" component={UserScreen} />
      </Navigator>
    </NavigationContainer>
  );
};
