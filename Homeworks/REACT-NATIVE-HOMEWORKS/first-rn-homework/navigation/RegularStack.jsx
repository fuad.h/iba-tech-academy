import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { RegularScreen } from "../screens/RegularScreen";
import { RegularLists } from "../screens";
export const RegularStack = () => {
  const { Navigator, Screen } = createStackNavigator();
  return (
    <Navigator headerMode="none">
      <Screen name="Regular List" component={RegularScreen} />
      <Screen name="RegularLists" component={RegularLists} />
    </Navigator>
  );
};
