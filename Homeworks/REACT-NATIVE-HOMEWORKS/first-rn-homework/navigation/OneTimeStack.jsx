import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { OneTimeScreen, OneTimeList } from "../screens";

const { Navigator, Screen } = createStackNavigator();
export const OneTimeStack = () => (
  <Navigator headerMode="none">
    <Screen name="One Time List" component={OneTimeScreen} />
    <Screen name="One Time Items" component={OneTimeList} />
  </Navigator>
);
