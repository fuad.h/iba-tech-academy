import React, { useState } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { connect } from "react-redux";

import { MainLayout } from "../../commons/MainLayout";
import images from "../../style/image";
import {
  CustomButton,
  CustomText,
  EditForm,
  SingleListItem,
} from "../../components";

import {
  selectOneTimes,
  addOneTimeItem,
  toggleBoughtStatus,
  resetBought,
  updateOneTimeItem,
  deleteOneTimeItem,
} from "../../store/oneTime";

const mapStateToProps = (state) => ({
  oneTimeList: selectOneTimes(state),
});

export const OneTimeList = connect(mapStateToProps, {
  addOneTimeItem,
  toggleBoughtStatus,
  resetBought,
  updateOneTimeItem,
  deleteOneTimeItem,
})(
  ({
    route,
    oneTimeList,
    navigation,
    addOneTimeItem,
    toggleBoughtStatus,
    resetBought,
    updateOneTimeItem,
    deleteOneTimeItem,
  }) => {
    const [isEdit, setIsEdit] = useState(false);
    const emptyFields = {
      name: "",
      count: 1,
      unit: "",
      id: "",
    };
    const [fields, setFields] = useState(emptyFields);
    const [singleEdit, setSingleEdit] = useState(false);

    const { oneTimeID } = route.params;
    const item = oneTimeList.find((item) => item.id === oneTimeID);
    const length = item.items.length;
    const bought = item.items.filter((item) => item.bought == true).length;

    const rightPressHandeler = () => {
      setIsEdit((val) => !val);
      setFields(emptyFields);
    };

    const addToListHandler = (name, unit) => {
      addOneTimeItem({ oneTimeID, name, unit });
      console.log("name", name, "unit", unit);
    };

    const fieldsHandeler = (name, unit, id) => {
      setSingleEdit(true);
      setFields({
        name,
        unit: unit.split(" ")[1],
        count: unit.slice(1, 2),
        id,
      });
    };

    const updateHandeler = (name, unit) => {
      updateOneTimeItem({
        oneTimeID,
        itemID: fields.id,
        name,
        unit,
      });
    };

    const deleteHandeler = (itemID) => {
      deleteOneTimeItem({ oneTimeID, itemID });
      setIsEdit(false);
    };

    return (
      <MainLayout
        title={item.name}
        leftImg={images.arrowBack}
        leftPress={navigation.goBack}
        rightImg={isEdit ? images.save : images.edit}
        rightPress={rightPressHandeler}
      >
        {!isEdit && (
          <View style={styles.row}>
            <CustomButton
              title="reset"
              style={styles.btn}
              fontSize={10}
              onPress={() => resetBought({ oneTimeID })}
            />
            <CustomText weight="medium" style={styles.label}>
              {bought}/{length}
            </CustomText>
          </View>
        )}

        {isEdit && (
          <EditForm
            setEdit={() => setIsEdit(false)}
            fields={fields}
            singleEdit={singleEdit}
            addToList={(field, itemUnit) => addToListHandler(field, itemUnit)}
            cancel={() => setSingleEdit(false)}
            update={(name, unit) => updateHandeler(name, unit)}
          />
        )}

        <FlatList
          data={item.items}
          renderItem={({ item }) => (
            <SingleListItem
              name={item.name}
              unit={item.unit}
              isEdit={isEdit}
              bought={item.bought}
              toggleBought={() =>
                toggleBoughtStatus({ oneTimeID, itemID: item.id })
              }
              singleEdit={() => fieldsHandeler(item.name, item.unit, item.id)}
              deleteItem={() => deleteHandeler(item.id)}
            />
          )}
        />
      </MainLayout>
    );
  }
);

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "93%",
  },
  btn: {
    paddingVertical: 4,
    width: "22%",
  },
  label: {},
});
