import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { connect } from "react-redux";

import { MainLayout } from "../../commons/MainLayout";
import images from "../../style/image";
import { selectOneTimes } from "../../store/oneTime";
import { ProgressBar } from "../../components";

const mapStateToProps = (state) => ({
  oneTimeList: selectOneTimes(state),
});

export const OneTimeScreen = connect(mapStateToProps)(
  ({ navigation, oneTimeList }) => {
    return (
      <MainLayout
        title="One Time List"
        rightImg={images.menu}
        rightPress={navigation.openDrawer}
      >
        <FlatList
          data={oneTimeList}
          renderItem={({ item }) => (
            <ProgressBar
              oneTime={true}
              title={item.name}
              length={item.items.length}
              bought={item.items.filter((item) => item.bought === true).length}
              onPress={() =>
                navigation.navigate("One Time Items", { oneTimeID: item.id })
              }
            />
          )}
        />
      </MainLayout>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
});
