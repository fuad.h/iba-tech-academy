import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from "react-native";
import { connect } from "react-redux";

import { MainLayout } from "../../commons/MainLayout";
import { CustomText } from "../../components/CustomText";
import COLORS from "../../style/colors";
import { CustomButton } from "../../components";
import { addRegularList } from "../../store/regulars";
import { addOneTimeList } from "../../store/oneTime";

export const NewListScreen = connect(null, { addRegularList, addOneTimeList })(
  ({ navigation, addRegularList, addOneTimeList }) => {
    const [field, setField] = useState("");
    const [listType, setListType] = useState("");

    const addListHandler = () => {
      if (field.trim() === "") return;
      if (listType === "regular") {
        addRegularList(field);
        navigation.navigate("Regular");
      } else if (listType === "oneTime") {
        addOneTimeList(field);
        navigation.navigate("One Time List");
      }
      resetSetting();
    };

    function resetSetting() {
      setField("");
      setListType("");
    }

    return (
      <MainLayout title="New List">
        <View style={styles.container}>
          <CustomText weight="bold" style={styles.label}>
            list name
          </CustomText>
          <TextInput
            value={field}
            style={styles.input}
            onChangeText={setField}
          />
        </View>
        <View style={styles.row}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => setListType("oneTime")}
          >
            <CustomText
              weight="bold"
              style={[
                styles.listType,
                { opacity: listType === "oneTime" ? 1 : 0.4 },
              ]}
            >
              One Time
            </CustomText>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btn}
            onPress={() => setListType("regular")}
          >
            <CustomText
              weight="bold"
              style={[
                styles.listType,
                { opacity: listType === "regular" ? 1 : 0.4 },
              ]}
            >
              Regular
            </CustomText>
          </TouchableOpacity>
        </View>
        <CustomButton title="create list" onPress={addListHandler} />
      </MainLayout>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  label: {
    fontSize: 12,
    opacity: 0.5,
    marginBottom: 5,
  },
  input: {
    width: "100%",
    backgroundColor: COLORS.lightGrey,
    borderRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: "center",
    fontFamily: "MontserratMedium",
    fontSize: 18,
    marginBottom: 15,
  },
  row: {
    flexDirection: "row",
    paddingVertical: 15,
    width: "100%",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  btn: {
    width: "45%",
    backgroundColor: COLORS.lightGrey,
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignItems: "center",
  },
  listType: {
    fontSize: 12,
  },
});
