import React, { useState } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { connect } from "react-redux";

import { MainLayout } from "../../commons";
import images from "../../style/image";
import {
  CustomButton,
  CustomText,
  SingleListItem,
  EditForm,
} from "../../components";
import {
  selectRegularList,
  resetBought,
  toggleBoughtStatus,
  addRegularItem,
  updateRegularItem,
  deleteRegularItem,
} from "../../store/regulars";

const mapStateToProps = (state) => ({
  regularList: selectRegularList(state),
});

export const RegularLists = connect(mapStateToProps, {
  resetBought,
  toggleBoughtStatus,
  addRegularItem,
  updateRegularItem,
  deleteRegularItem,
})(
  ({
    route,
    navigation,
    regularList,
    resetBought,
    toggleBoughtStatus,
    addRegularItem,
    updateRegularItem,
    deleteRegularItem,
  }) => {
    const [isEdit, setIsEdit] = useState(false);
    const [singleEdit, setSingleEdit] = useState(false);
    const emptyFields = {
      name: "",
      count: 1,
      unit: "",
      id: "",
    };
    const [fields, setFields] = useState(emptyFields);

    const { id } = route.params;
    const [item] = regularList.filter((item) => item.id == id);
    const length = item.items.length;
    const bought = item.items.filter((item) => item.bought == true).length;

    const fieldsHandeler = (name, unit, id) => {
      setSingleEdit(true);
      setFields({
        name,
        unit: unit.split(" ")[1],
        count: unit.slice(1, 2),
        id,
      });
    };

    const rightPressHandeler = () => {
      setIsEdit((val) => !val);
      setFields(emptyFields);
    };

    const updateHandeler = (name, unit) => {
      updateRegularItem({
        regularID: id,
        itemID: fields.id,
        name,
        unit,
      });
    };

    const deleteHandler = (itemID) => {
      deleteRegularItem({ regularID: id, itemID });
      setIsEdit(false);
    };

    return (
      <MainLayout
        title={item.name}
        rightImg={isEdit ? images.save : images.edit}
        leftImg={images.arrowBack}
        leftPress={() => navigation.goBack()}
        rightPress={rightPressHandeler}
      >
        {!isEdit && (
          <View style={styles.row}>
            <CustomButton
              title="reset"
              style={styles.btn}
              fontSize={10}
              onPress={() => resetBought({ regularID: id })}
            />
            <CustomText weight="medium" style={styles.label}>
              {bought}/{length}
            </CustomText>
          </View>
        )}

        {isEdit && (
          <EditForm
            singleEdit={singleEdit}
            addToList={(name, itemUnit) =>
              addRegularItem({ regularID: id, name, unit: itemUnit })
            }
            fields={fields}
            cancel={() => setSingleEdit(false)}
            update={(name, unit) => updateHandeler(name, unit)}
          />
        )}

        <FlatList
          data={item.items}
          renderItem={({ item }) => (
            <SingleListItem
              name={item.name}
              unit={item.unit}
              isEdit={isEdit}
              bought={item.bought}
              toggleBought={() =>
                toggleBoughtStatus({ regularID: id, itemID: item.id })
              }
              singleEdit={() => fieldsHandeler(item.name, item.unit, item.id)}
              deleteItem={() => deleteHandler(item.id)}
            />
          )}
        />
      </MainLayout>
    );
  }
);

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "93%",
  },
  btn: {
    paddingVertical: 4,
    width: "22%",
  },
  label: {},
});
