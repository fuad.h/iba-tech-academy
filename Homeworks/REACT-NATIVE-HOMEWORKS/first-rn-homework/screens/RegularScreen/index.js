import React from "react";
import { FlatList } from "react-native";
import { connect } from "react-redux";

import images from "../../style/image";
import { MainLayout } from "../../commons";
import { selectRegularList } from "../../store/regulars";
import { ProgressBar } from "../../components/ProgressBar";

const mapStateToProps = (state) => ({
  regularList: selectRegularList(state),
});

export const RegularScreen = connect(mapStateToProps)(
  ({ navigation, regularList }) => {
    return (
      <MainLayout
        title="Regular Lists"
        rightImg={images.menu}
        rightPress={navigation.openDrawer}
      >
        <FlatList
          data={regularList}
          renderItem={({ item }) => {
            const length = item.items.length;
            const bought = item.items.filter((item) => item.bought == true)
              .length;
            return (
              <ProgressBar
                title={item.name}
                length={length}
                bought={bought}
                onPress={() =>
                  navigation.navigate("RegularLists", {
                    id: item.id,
                  })
                }
              />
            );
          }}
        />
      </MainLayout>
    );
  }
);
