import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import { connect } from "react-redux";

import { MainLayout } from "../../commons/MainLayout";
import { CustomText } from "../../components/CustomText";
import COLORS from "../../style/colors";
import { addUser, selectUser } from "../../store/user";
import { CustomButton } from "../../components";

const mapStateToProps = (state) => ({
  user: selectUser(state),
});

export const UserScreen = connect(mapStateToProps, { addUser })(
  ({ user, addUser, navigation }) => {
    const [fields, setFields] = useState({
      name: user?.name || "",
      imgUri: user?.imgUri || "",
    });

    const fieldHandler = (name, val) => {
      setFields((field) => ({
        ...field,
        [name]: val,
      }));
    };

    const addUserHandler = () => {
      addUser({ name: fields.name, imgUri: fields.imgUri });
      Keyboard.dismiss();
      navigation.navigate("Regular");
      navigation.openDrawer();
    };
    return (
      <MainLayout title="User Profile">
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={styles.container}>
            <CustomText weight="bold" style={styles.label}>
              username
            </CustomText>
            <TextInput
              value={fields.name}
              style={styles.input}
              placeholder="profile name"
              onChangeText={(val) => fieldHandler("name", val)}
            />

            <CustomText weight="bold" style={styles.label}>
              avatarurl
            </CustomText>
            <TextInput
              value={fields.imgUri}
              style={styles.input}
              placeholder="image url"
              onChangeText={(val) => fieldHandler("imgUri", val)}
            />
            <CustomButton title="save changes" onPress={addUserHandler} />
          </View>
        </TouchableWithoutFeedback>
      </MainLayout>
    );
  }
);

export const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  label: {
    fontSize: 12,
    opacity: 0.5,
    marginBottom: 5,
  },
  input: {
    width: "100%",
    backgroundColor: COLORS.lightGrey,
    borderRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 15,
    textAlign: "center",
    fontFamily: "MontserratMedium",
    fontSize: 18,
    marginBottom: 15,
  },
});
