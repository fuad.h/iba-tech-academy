import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";

import { CustomText } from "./CustomText";
import { CustomButton } from "./CustomButton";

export const EditForm = ({
  setEdit,
  singleEdit,
  addToList,
  fields,
  cancel,
  update,
}) => {
  const [unit, setUnit] = useState(fields?.unit || "");
  const [count, setCount] = useState(fields?.count || 1);
  const [field, setField] = useState(fields?.name || "");
  const [opacity, setOpacity] = useState(0.5);

  useEffect(() => {
    setUnit(fields?.unit || "");
    setField(fields?.name || "");
    setCount(+fields?.count || 1);
  }, [fields]);

  useEffect(() => {
    if (fields?.unit) {
      if (
        unit !== fields.unit ||
        count !== +fields.count ||
        field !== fields.name
      )
        setOpacity(1);
    }
  }, [unit, count, field]);

  const countHandler = (name) => {
    if (name === "sub") {
      if (count > 1) {
        setCount((val) => val - 1);
      }
    } else if (name === "sum") {
      setCount((val) => val + 1);
    }
  };

  const reset = () => {
    setField("");
    setCount(1);
    setUnit("");
  };

  const addToListHandeler = () => {
    if (field.trim() !== "" && unit !== "") {
      const itemUnit = `x${count} ${unit}`;
      addToList(field, itemUnit);
      reset();
    }
    Keyboard.dismiss();
  };

  const cancelEdit = () => {
    cancel();
    reset();
  };

  const updateHandeler = () => {
    update(field, `x${count} ${unit}`);
    setEdit();
  };

  const units = {
    kg: "kg",
    litre: "litre",
    pkg: "pkg",
    bott: "bott",
  };

  const unitList = [];
  for (let key in units) {
    const unitKey = units[key];
    unitList.push(
      <TouchableOpacity key={unitKey} onPress={() => setUnit(unitKey)}>
        <View style={[styles.unit, { opacity: unit === unitKey ? 1 : 0.5 }]}>
          <CustomText weight="bold">{unitKey}</CustomText>
        </View>
      </TouchableOpacity>
    );
  }

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.container}>
          <View style={styles.row}>
            <View style={styles.name}>
              <CustomText weight="bold" style={styles.label}>
                postion name
              </CustomText>
              <TextInput
                value={field}
                onChangeText={(val) => setField(val)}
                style={styles.nameInput}
              />
            </View>

            <View style={styles.count}>
              <CustomText weight="bold" style={styles.label}>
                count
              </CustomText>
              <View style={styles.countWrapper}>
                <TouchableOpacity onPress={() => countHandler("sub")}>
                  <CustomText weight="bold" style={styles.countLabel}>
                    -
                  </CustomText>
                </TouchableOpacity>
                <CustomText weight="bold" style={styles.countLabel}>
                  {count}
                </CustomText>
                <TouchableOpacity onPress={() => countHandler("sum")}>
                  <CustomText weight="bold" style={styles.countLabel}>
                    +
                  </CustomText>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.unitsRow}>{unitList}</View>

          {!singleEdit && (
            <CustomButton
              title="add to list"
              style={styles.btnAdd}
              onPress={addToListHandeler}
            />
          )}

          {singleEdit && (
            <View style={styles.btnWrapper}>
              <View style={[styles.btn, { opacity: opacity }]}>
                <CustomButton title="cancel" onPress={cancelEdit} />
              </View>

              <View style={styles.btn}>
                <CustomButton title="update" onPress={updateHandeler} />
              </View>
            </View>
          )}

          <View style={styles.line} />
        </View>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  name: {
    alignItems: "center",
    width: "70%",
  },
  label: {
    fontSize: 12,
    opacity: 0.5,
  },
  nameInput: {
    backgroundColor: "#eee",
    borderRadius: 30,
    width: "100%",
    paddingVertical: 8,
    textAlign: "center",
    fontFamily: "MontserratBold",
    fontSize: 14,
  },
  count: {
    width: "25%",
    alignItems: "center",
  },
  countWrapper: {
    flexDirection: "row",
    width: "100%",
    paddingVertical: 12,
    backgroundColor: "#eee",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  countLabel: {
    fontSize: 16,
    paddingHorizontal: 5,
  },
  unitsRow: {
    flexDirection: "row",
    marginVertical: 15,
  },
  unit: {
    backgroundColor: "#eee",
    paddingVertical: 8,
    paddingHorizontal: 25,
    borderRadius: 30,
    marginRight: 20,
  },
  btnAdd: {
    marginVertical: 20,
  },
  line: {
    backgroundColor: "#eee",
    height: 5,
    marginHorizontal: -15,
  },
  btnWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    marginVertical: 20,
  },
  btn: {
    width: "45%",
  },
});
