import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import { CustomText } from "./CustomText";
import COLORS from "../style/colors";

export const ProgressBar = ({ title, length, bought, oneTime, ...rest }) => {
  let width;
  if (length == 0) {
    width = 0;
  } else {
    width = Math.floor((bought / length) * 100);
  }

  const isFull =
    oneTime && (bought > 0 ? (bought === length ? true : false) : false);

  return (
    <TouchableOpacity {...rest}>
      <View style={[styles.container, { opacity: isFull ? 0.5 : 1 }]}>
        <View style={styles.row}>
          <CustomText weight="bold" style={styles.label}>
            {title}
          </CustomText>
          <CustomText weight="medium" style={styles.numbersLabel}>
            {bought}/{length}
          </CustomText>
        </View>

        <View style={styles.progressWrapper}>
          <View style={[styles.progress, { width: `${width} %` }]} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderRadius: 8,
    borderWidth: 1.5,
    borderColor: COLORS.yellow,
    backgroundColor: "#fff",
    paddingHorizontal: 20,
    paddingVertical: 18,
    marginBottom: 15,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  label: {
    fontSize: 18,
  },
  numbersLabel: {
    fontSize: 14,
  },
  progressWrapper: {
    width: "100%",
    borderRadius: 30,
    backgroundColor: COLORS.lightGrey,
    height: 20,
    marginTop: 10,
    justifyContent: "flex-start",
  },
  progress: {
    backgroundColor: COLORS.yellow,
    height: 20,
    borderRadius: 30,
  },
});
