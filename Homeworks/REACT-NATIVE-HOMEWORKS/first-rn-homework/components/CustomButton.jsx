import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import { CustomText } from "./CustomText";
import COLORS from "../style/colors";

export const CustomButton = ({
  title,
  style,
  fontSize,
  textColor,
  ...rest
}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity {...rest}>
        <View style={[styles.btn, style]}>
          <CustomText
            weight="bold"
            style={[
              styles.text,
              { fontSize: fontSize },
              { color: textColor || "white" },
            ]}
          >
            {title}
          </CustomText>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderRadius: 30,
    overflow: "hidden",
  },
  btn: {
    borderRadius: 30,
    backgroundColor: COLORS.main,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10,
  },
  text: {
    textTransform: "uppercase",
    fontSize: 14,
    color: "white",
  },
});
