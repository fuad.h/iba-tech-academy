import React from "react";
import { StyleSheet, View, Image, TouchableOpacity, Alert } from "react-native";

import { CustomText } from "./CustomText";
import images from "../style/image";
import COLORS from "../style/colors";

export const SingleListItem = ({
  isEdit,
  name,
  unit,
  bought,
  toggleBought,
  singleEdit,
  deleteItem,
}) => {
  const deleteHandler = () => {
    Alert.alert(
      "Are you sure to delete this item?",
      "We are suggest not to delete",
      [
        {
          text: "ok",
          onPress: deleteItem,
          style: "destructive",
        },
        {
          text: "cancel",
          style: "cancel",
        },
      ]
    );
  };
  return (
    <TouchableOpacity onLongPress={toggleBought}>
      <View
        style={[
          styles.container,
          { paddingVertical: isEdit ? 0 : 10 },
          { opacity: bought ? 0.4 : 1 },
        ]}
      >
        <View style={styles.row}>
          {isEdit && (
            <TouchableOpacity onPress={singleEdit}>
              <View style={styles.circle}>
                <Image
                  resizeMode="contain"
                  style={styles.img}
                  source={images.edit}
                />
              </View>
            </TouchableOpacity>
          )}
          <CustomText weight="medium" style={styles.label}>
            {name}
          </CustomText>
        </View>

        <View style={styles.row}>
          <CustomText weight="medium" style={styles.label}>
            {unit}
          </CustomText>
          {isEdit && (
            <TouchableOpacity onPress={deleteHandler}>
              <View style={[styles.circle, { backgroundColor: COLORS.main }]}>
                <Image
                  resizeMode="contain"
                  style={styles.img}
                  source={images.remove}
                />
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderRadius: 30,
    borderWidth: 1,
    borderColor: COLORS.yellow,
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 12,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    overflow: "hidden",
  },
  circle: {
    backgroundColor: COLORS.yellow,
    height: 40,
    width: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  img: {
    width: 20,
    height: 30,
  },
  label: {
    paddingHorizontal: 15,
  },
});
