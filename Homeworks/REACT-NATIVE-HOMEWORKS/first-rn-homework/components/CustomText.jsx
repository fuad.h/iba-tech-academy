import React from "react";
import { Text } from "react-native";

export const CustomText = ({ children, weight, style, fontSize, ...rest }) => {
  const Families = {
    regular: "MontserratRegular",
    bold: "MontserratBold",
    medium: "MontserratMedium",
  };
  return (
    <Text
      {...rest}
      style={[
        { fontFamily: Families[weight] || Families.regular },
        { fontSize: 14 },
        style,
        ,
      ]}
    >
      {children}
    </Text>
  );
};
