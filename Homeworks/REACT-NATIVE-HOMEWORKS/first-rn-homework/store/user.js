const ADD_USER = "ADD_USER";

export const MODEL_NAME = "user";
export const selectUser = (state) => state[MODEL_NAME].user;

const initialState = {
  user: {
    imgUri:
      "https://lh3.googleusercontent.com/zvuqfxWWv581MWVTlstjf66dO868Olsa1GJcMzYoKQs_wt6jnN5BHRbnhdbKG5XK_F0ZuA",
  },
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_USER:
      return {
        ...state,
        user: {
          name: payload.name,
          imgUri: payload.imgUri,
        },
      };
    default:
      return state;
  }
}

export const addUser = (payload) => ({
  type: ADD_USER,
  payload,
});
