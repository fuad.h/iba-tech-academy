import { createID } from "./regulars";

const ADD_ONETIME_LIST = "ADD_ONETIME_LIST";
const ADD_ONETIME_ITEM = "ADD_ONETIME_ITEM";
const TOGGLE_BOUGHT_STATUS = "TOGGLE_BOUGHT_STATUS";
const RESET_BOUGHT = "RESET_BOUGHT";
const UPDATE_ONETIME_ITEM = "UPDATE_ONETIME_ITEM";
const DELETE_ONETIME_ITEM = "DELETE_ONETIME_ITEM";

export const MODEL_NAME = "oneTime";
export const selectOneTimes = (state) => state[MODEL_NAME].list;

const initialState = {
  list: [],
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_ONETIME_LIST:
      return {
        ...state,
        list: [
          {
            id: createID(),
            name: payload,
            items: [],
          },
          ...state.list,
        ],
      };

    case ADD_ONETIME_ITEM:
      return {
        ...state,
        list: state.list.map((oneTime) => {
          if (oneTime.id === payload.oneTimeID) {
            return {
              ...oneTime,
              items: [
                {
                  id: createID(),
                  name: payload.name,
                  unit: payload.unit,
                  bought: false,
                },
                ...oneTime.items,
              ],
            };
          }
          return oneTime;
        }),
      };

    case TOGGLE_BOUGHT_STATUS:
      return {
        ...state,
        list: state.list.map((oneTime) => {
          if (oneTime.id == payload.oneTimeID) {
            return {
              ...oneTime,
              items: oneTime.items.map((item) => {
                if (item.id == payload.itemID) {
                  return {
                    ...item,
                    bought: !item.bought,
                  };
                }
                return item;
              }),
            };
          }
          return oneTime;
        }),
      };

    case RESET_BOUGHT:
      return {
        ...state,
        list: state.list.map((oneTime) => {
          if (oneTime.id === payload.oneTimeID) {
            return {
              ...oneTime,
              items: oneTime.items.map((item) => {
                if (item.bought == true) {
                  return {
                    ...item,
                    bought: false,
                  };
                }
                return item;
              }),
            };
          }
          return oneTime;
        }),
      };

    case UPDATE_ONETIME_ITEM:
      return {
        ...state,
        list: state.list.map((oneTime) => {
          if (oneTime.id == payload.oneTimeID) {
            return {
              ...oneTime,
              items: oneTime.items.map((item) => {
                if (item.id == payload.itemID) {
                  return {
                    ...item,
                    name: payload.name,
                    unit: payload.unit,
                  };
                }
                return item;
              }),
            };
          }
          return oneTime;
        }),
      };

    case DELETE_ONETIME_ITEM:
      return {
        ...state,
        list: state.list.map((oneTime) => {
          if (oneTime.id == payload.oneTimeID) {
            return {
              ...oneTime,
              items: oneTime.items.filter((item) => item.id !== payload.itemID),
            };
          }
          return oneTime;
        }),
      };

    default:
      return state;
  }
}

export const addOneTimeList = (payload) => ({
  type: ADD_ONETIME_LIST,
  payload,
});

export const addOneTimeItem = (payload) => ({
  type: ADD_ONETIME_ITEM,
  payload,
});

export const toggleBoughtStatus = (payload) => ({
  type: TOGGLE_BOUGHT_STATUS,
  payload,
});

export const resetBought = (payload) => ({
  type: RESET_BOUGHT,
  payload,
});

export const updateOneTimeItem = (payload) => ({
  type: UPDATE_ONETIME_ITEM,
  payload,
});

export const deleteOneTimeItem = (payload) => ({
  type: DELETE_ONETIME_ITEM,
  payload,
});
