import { createStore, combineReducers } from "redux";

import {
  MODEL_NAME as regularModelName,
  reducer as regularReducer,
} from "./regulars";
import { MODEL_NAME as userModelName, reducer as userReducer } from "./user";
import {
  MODEL_NAME as oneTimeModelName,
  reducer as oneTimeReduer,
} from "./oneTime";

const rootReducer = combineReducers({
  [regularModelName]: regularReducer,
  [userModelName]: userReducer,
  [oneTimeModelName]: oneTimeReduer,
});

const store = createStore(rootReducer);

export default store;
