const ADD_REGULAR_LIST = "ADD_REGULAR_LIST";
const TOGGLE_BOUGHT_STATUS = "TOGGLE_BOUGHT_STATUS";
const RESET_BOUGHT = "RESET_BOUGHT";
const ADD_REGULAR_ITEM = "ADD_REGULAR_ITEM";
const UPDATE_REGULAR_ITEM = "UPDATE_REGULAR_ITEM";
const DELETE_REGULAR_ITEM = "DELETE_REGULAR_ITEM";

export const MODEL_NAME = "posts";
export const selectRegularList = (state) => state[MODEL_NAME].list;

const initialState = {
  list: [
    {
      id: createID(),
      name: "Everything for breakfast",
      items: [
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: false,
        },
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: true,
        },
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: false,
        },
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: true,
        },
      ],
    },
    {
      id: createID(),
      name: "Evening for breakfast",
      items: [
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: false,
        },
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: false,
        },
        {
          id: createID(),
          name: "Pasta",
          unit: "x2 kg",
          bought: true,
        },
      ],
    },
  ],
};

export function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ADD_REGULAR_LIST:
      return {
        ...state,
        list: [
          {
            id: createID(),
            name: payload,
            items: [],
          },
          ...state.list,
        ],
      };

    case TOGGLE_BOUGHT_STATUS:
      return {
        ...state,
        list: state.list.map((regular) => {
          if (regular.id == payload.regularID) {
            return {
              ...regular,
              items: regular.items.map((item) => {
                if (item.id == payload.itemID) {
                  return {
                    ...item,
                    bought: !item.bought,
                  };
                }
                return item;
              }),
            };
          }
          return regular;
        }),
      };

    case DELETE_REGULAR_ITEM:
      return {
        ...state,
        list: state.list.map((regular) => {
          if (regular.id == payload.regularID) {
            return {
              ...regular,
              items: regular.items.filter((item) => item.id !== payload.itemID),
            };
          }
          return regular;
        }),
      };

    case UPDATE_REGULAR_ITEM:
      return {
        ...state,
        list: state.list.map((regular) => {
          if (regular.id == payload.regularID) {
            return {
              ...regular,
              items: regular.items.map((item) => {
                if (item.id == payload.itemID) {
                  return {
                    ...item,
                    name: payload.name,
                    unit: payload.unit,
                  };
                }
                return item;
              }),
            };
          }
          return regular;
        }),
      };

    case ADD_REGULAR_ITEM:
      return {
        ...state,
        list: state.list.map((regular) => {
          if (regular.id == payload.regularID) {
            return {
              ...regular,
              items: [
                {
                  id: createID(),
                  name: payload.name,
                  unit: payload.unit,
                  bought: false,
                },
                ...regular.items,
              ],
            };
          }
          return regular;
        }),
      };

    case RESET_BOUGHT:
      return {
        ...state,
        list: state.list.map((regular) => {
          if (regular.id === payload.regularID) {
            return {
              ...regular,
              items: regular.items.map((item) => {
                if (item.bought == true) {
                  return {
                    ...item,
                    bought: false,
                  };
                }
                return item;
              }),
            };
          }
          return regular;
        }),
      };

    default:
      return state;
  }
}

export const addRegularList = (payload) => ({
  type: ADD_REGULAR_LIST,
  payload,
});

export const toggleBoughtStatus = (payload) => ({
  type: TOGGLE_BOUGHT_STATUS,
  payload,
});

export const resetBought = (payload) => ({
  type: RESET_BOUGHT,
  payload,
});

export const addRegularItem = (payload) => ({
  type: ADD_REGULAR_ITEM,
  payload,
});

export const updateRegularItem = (payload) => ({
  type: UPDATE_REGULAR_ITEM,
  payload,
});

export const deleteRegularItem = (payload) => ({
  type: DELETE_REGULAR_ITEM,
  payload,
});

export function createID() {
  return `${Date.now()}${Math.random()}`;
}
