import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { AppLoading } from "expo";
import { Provider } from "react-redux";

import { loadFonts } from "./style/font";
import { RootDrawer } from "./navigation";
import store from "./store";

export default function App() {
  const [loaded, setLoaded] = useState(false);

  if (!loaded) {
    return (
      <AppLoading
        startAsync={loadFonts}
        onFinish={() => setLoaded(true)}
        onError={() => console.log("Error on loading")}
      />
    );
  } else {
    return (
      <Provider store={store}>
        <RootDrawer />
      </Provider>
    );
  }
}
