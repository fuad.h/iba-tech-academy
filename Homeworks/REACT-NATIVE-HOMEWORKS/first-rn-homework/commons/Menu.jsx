import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { useSafeArea } from "react-native-safe-area-context";
import { connect } from "react-redux";

import { CustomText, CustomButton } from "../components";
import images from "../style/image";
import COLORS from "../style/colors";
import { selectUser } from "../store/user";
const mapStateToProps = (state) => ({
  user: selectUser(state),
});

export const Menu = connect(mapStateToProps)(({ navigation, user }) => {
  const { top } = useSafeArea();
  const userName = user?.name || "User name";

  const bntTitle = {
    oneTime: "One Time List",
    regular: "Regular List",
    user: "User Profile",
  };

  let btnArr = [];
  for (let key in bntTitle) {
    const name = bntTitle[key];
    btnArr.push(
      <CustomButton
        key={key}
        style={styles.btn}
        title={name}
        textColor={COLORS.main}
        onPress={() => navigation.navigate(name)}
      />
    );
  }

  return (
    <View style={[styles.container, { marginTop: top }]}>
      <View style={styles.row}>
        <View style={styles.imgWrapper}>
          <Image
            resizeMode="stretch"
            style={styles.img}
            source={user?.imgUri ? { uri: user.imgUri } : images.userEmpty}
          />
        </View>
        <CustomText style={styles.name}>{userName}</CustomText>
      </View>

      <View style={styles.content}>
        <CustomButton
          style={styles.btn}
          title="add new list"
          textColor={COLORS.main}
          onPress={() => navigation.navigate("New List")}
        />
        <View style={styles.bntRow}>{btnArr}</View>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  row: {
    flexDirection: "row",
    paddingVertical: 15,
    alignItems: "center",
  },
  imgWrapper: {
    width: 50,
    height: 50,
    borderWidth: 2,
    borderColor: COLORS.main,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#eee",
    marginHorizontal: 15,
    overflow: "hidden",
  },
  img: {
    width: 45,
    height: 50,
  },
  name: {
    fontSize: 24,
  },
  content: {
    backgroundColor: COLORS.main,
    borderTopStartRadius: 27,
    borderTopEndRadius: 27,
    paddingVertical: 15,
    paddingHorizontal: 15,
    flex: 1,
  },
  btn: {
    backgroundColor: "#fff",
    marginVertical: 7,
  },
  bntRow: {
    marginTop: 25,
  },
});
