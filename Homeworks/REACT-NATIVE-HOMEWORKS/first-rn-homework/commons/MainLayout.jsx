import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
} from "react-native";
import { useSafeArea } from "react-native-safe-area-context";

import COLORS from "../style/colors";
import { CustomText } from "../components/CustomText";

export const MainLayout = ({
  children,
  title,
  rightImg,
  leftImg,
  rightPress,
  leftPress,
}) => {
  const { top } = useSafeArea();
  return (
    <View style={[styles.container, { marginTop: top }]}>
      <StatusBar
        backgroundColor={COLORS.main}
        translucent={true}
        barStyle="light-content"
      />
      <View style={styles.row}>
        <View style={styles.imgWrapper}>
          {leftImg && (
            <TouchableOpacity onPress={leftPress}>
              <Image resizeMode="contain" style={styles.img} source={leftImg} />
            </TouchableOpacity>
          )}
        </View>

        <CustomText weight="medium" style={styles.title}>
          {title}
        </CustomText>

        <View style={styles.imgWrapper}>
          {rightImg && (
            <TouchableOpacity onPress={rightPress}>
              <Image
                resizeMode="contain"
                style={styles.img}
                source={rightImg}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>

      <View style={styles.content}>{children}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.main,
  },
  content: {
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: "white",
    borderTopStartRadius: 27,
    borderTopEndRadius: 27,
    paddingTop: 20,
  },
  row: {
    height: 70,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 15,
    alignItems: "center",
  },
  imgWrapper: {
    zIndex: 20,
    height: 30,
    width: 21,
  },
  img: {
    width: 21,
    height: 30,
  },
  title: {
    color: "white",
    fontSize: 18,
  },
});
