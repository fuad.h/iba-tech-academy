const COLORS = {
  main: "#FF7676",
  fadedYellow: "#FFE194",
  yellow: "#FFD976",
  black: "#303234",
  lightGrey: "#EEEEEE",
};

export default COLORS;
