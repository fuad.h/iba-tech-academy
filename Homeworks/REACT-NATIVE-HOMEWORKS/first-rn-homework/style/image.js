import menu from "../assets/menu.png";
import arrowBack from "../assets/arrow_back.png";
import edit from "../assets/edit.png";
import save from "../assets/save.png";
import remove from "../assets/remove.png";
import userEmpty from "../assets/user_empty.png";

const images = {
  menu,
  arrowBack,
  edit,
  remove,
  save,
  userEmpty,
};

export default images;
